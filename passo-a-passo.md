 #### Sistema utilizado: WSL Ubuntu
 1. Entrei no usuário root com o comando `sudo su`.
 2. fui ate a pasta `/home`.
 3. Criei o arquivo .txt utilizando comando touch.
 4. Adicionei o conteúdo "Nome e comida preferida" ao arquivo com o comando `echo` e os operador `>`.
 5. Redirecionei a saída do comando `dmesg` para `text.txt` utilizando operador `>>`.
 6. Rodei o comando `ls /etc/netplan` para analisar o arquivo de configuração de rede, como estou utilizando o WSL alguns arquivos/diretórios não existem, devido a isso, criei um arquivo
    chamado `network.yaml` para setar as conf de rede dentro do diretório acima.
 7. Setei as configurações de rede assim como especificado no documento
    utilizando o editor nano.
 8. Fiz a copia das 10 ultimas linhas do arquivo `rsyslog.conf` para o
    arquivo test.txt utilizando o comando `tail /etc/rsyslog.conf >> test.txt`, 
    obs: Não encontrei o arquivo `syslog` muito provavelmente por estar utilizando o WSL.
 9. Criei o usuário **Elliot** com o comando `useradd -m "user"` e definir a
    senha com o comando `passwd "user"`.
 10. Criei o grupo **fsociety** com o comando `addgroup` e definir a senha com
     o comando `gpasswd`.
 11. Adicionei o usuário **Elliot** ao grupo **fsociety** com o comando `usermod-aG "group" "user"` e utilizei 			          o comando `groups "user"` para confirmar que o usuário estava no grupo de fato.
 12. Criei o usuário **darlen** e adicionei o mesmo ao grupo **fsociety**.
 13. Alterei o ID do usuário **darlen** para 4566 utilizando o comando `usermod -u "id" "user"`.
 14. Copiei as 7 primeiras linhas do arquivo `rsyslog.conf` com o comando `head -7 /etc/rsyslog.conf >> test.txt` devido a estar utilizando WSL alguns arquivos não estão disponíveis.
 15. Adicionei a frase no final do arquivo de texto utilizando `echo "Segunda Fase" >> test.txt`.
##### Material de Apoio
https://guialinux.uniriotec.br/
https://sempreupdate.com.br/
https://www.hostinger.com.br/
https://diolinux.com.br/
